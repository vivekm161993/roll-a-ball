﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	private Rigidbody rb;
	public float Speed;
	private int count;	
	public Text textCount;
	public Text winText;

	void Start ()
	{	count = 0;
		rb = GetComponent<Rigidbody> ();
		UpdateCount ();
		winText.text = "";
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * Speed);

	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.CompareTag("Pick Up")) 
		{
			count = count + 1;
			collider.gameObject.SetActive (false);
			UpdateCount ();
		}
		if (count >= 14) {
			winText.text = "You Win !";
		}

	}
	void UpdateCount(){
		textCount.text = "Count : "+count.ToString ();
	}
}
